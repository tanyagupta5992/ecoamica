import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-approvals',
  templateUrl: './approvals.component.html',
  styleUrls: ['./approvals.component.css']
})
export class ApprovalsComponent implements OnInit {

  constructor(private http:HttpClient) { }

  proofList:any;
  newList:any;
  hasProofs:boolean;
  showConfirmDialog:boolean;
  status='';

  ngOnInit() {
    this.getProofs();
  }

  getProofs() {
    this.hasProofs=false;
    this.newList=[];
    this.http.get('http://localhost:8080/eco-amica-0.0.1-SNAPSHOT/service/getFilesForReview')
    .subscribe(res => {
      this.proofList=res;
      for(let proof of this.proofList) {
        if(proof.status != 'Approved' && proof.status != 'Rejected') {
          this.newList.push(proof);
        }
      }
      if(this.newList.length>0) {
        this.hasProofs=true;
      }
    })
  }

  downloadFile(attachment) {
    window.open('http://localhost:8080/eco-amica-0.0.1-SNAPSHOT/service/downloadFile/'+attachment.attachment_id,attachment.fileName);
  }

  update(status,id) {
    let submitData={
      attachmentId:id,
      updatedStatus:status
    }
    this.http.post('http://localhost:8080/eco-amica-0.0.1-SNAPSHOT/service/updateStatus/'+id+'/'+status,submitData)
    .subscribe(res => {
      this.getProofs();
      if(status=='Approved') {
        this.status = 'Approval Successful!';
      }
      else {
        this.status = 'Rejection Successful!';
      }
      this.showConfirmDialog=true;
    })
  }

  hideFileConfirmDialog() {
    this.showConfirmDialog=false;
  }

}
