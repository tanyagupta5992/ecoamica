import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { GreenmeasuresComponent } from './greenmeasures/greenmeasures.component';
import { ApprovalsComponent } from './approvals/approvals.component';
import { ConsumptionDataComponent } from './consumption-data/consumption-data.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'greenmeasures', component:GreenmeasuresComponent},
  { path: 'approvals', component:ApprovalsComponent},
  { path: 'consumption', component:ConsumptionDataComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }