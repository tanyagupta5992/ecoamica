import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';


@Component({
  selector: 'app-greenmeasures',
  templateUrl: './greenmeasures.component.html',
  styleUrls: ['./greenmeasures.component.css']
})
export class GreenmeasuresComponent implements OnInit {

  constructor(private http:HttpClient) {}

  display:'';
  level:'';
  level1Completed:boolean=true;
  level2Completed:boolean=true;
  level3Completed:boolean=true;
  level1Approved:boolean;
  level2Approved:boolean;
  level3Approved:boolean;
  level1Count=3;
  level2Count=3;
  level3Count=3;
  level1TotalActions=0;
  level2TotalActions=0;
  level3TotalActions=0;
  level1TotalActionsApproved=0;
  level2TotalActionsApproved=0;
  level3TotalActionsApproved=0;
  levelSelected='';
  proofLevelSelected='';
  displayModal:boolean;
  actions:any;
  actionStatus:any;
  actionCompletedDialog:boolean;
  actionCompletedPrev:boolean;
  showFileConfirm:boolean;
  attachmentsList:any;
  showAttachments:boolean;
  viewProofModal:boolean;
  viewLevelProofs='';
  hotelCode='TESTA';

  ngOnInit() {
    this.levelSelected='Level 1';
    this.getAllActions();
  }

  getAllActions() {
    this.level1Count=3;
    this.level2Count=3;
    this.level3Count=3;
    this.level1TotalActions=0;
    this.level2TotalActions=0;
    this.level3TotalActions=0;
    this.level1TotalActionsApproved=0;
    this.level2TotalActionsApproved=0;
    this.level3TotalActionsApproved=0;
    this.level1Completed=true;
    this.level2Completed=true;
    this.level3Completed=true;
    this.level1Approved=false;
    this.level2Approved=false;
    this.level3Approved=false;
    this.http.get('http://localhost:8080/eco-amica-0.0.1-SNAPSHOT/service/all/plans')
    .subscribe(res => {
      this.actions = res;
      for(let action of this.actions) {
        let codeLetter1 = action.levelType.substring(0,1);
        let codeLetter2 = action.levelType.substring(2,3);
        let codeLetter3 = action.levelType.substring(4,5);
        action.code = (codeLetter1+codeLetter2+codeLetter3).toLowerCase();
        action.levelCode = (action.code + action.levelInfo).replace(/\s/g, "");
        action.proofChecked = false;    
      }
      this.getAllAttachments();
    })
  }

  getAllAttachments() {
    this.http.get('http://localhost:8080/eco-amica-0.0.1-SNAPSHOT/service/getAttachmentDetails/'+this.hotelCode)
    .subscribe(res => {
       this.attachmentsList=res;
       if(this.attachmentsList.length>0) {
         this.showAttachments=true;
       }
       for(let attachment of this.attachmentsList) {
         for(let action of this.actions) {
           if(attachment.levelType == action.levelType) {
             action.fileUploaded=true;
             action.attachmentId=attachment.attachment_id;
             if(attachment.status == 'Approved') {
               action.proofApproved=true;
             }
             if(attachment.status == 'Rejected') {
               action.proofRejected=true;
             }
           }
         }
        }
        this.getActionStatus();
    })
  }

  getActionStatus() {
    this.http.get('http://localhost:8080/eco-amica-0.0.1-SNAPSHOT/service/measures/'+this.hotelCode)
    .subscribe(res => {
      this.actionStatus = res;
      for(let action of this.actions) {
        for(let status in this.actionStatus) {
          if(status == action.levelCode) {
            action['completed']=this.actionStatus[status];
            if(action.levelInfo=='Level 1') {
              this.level1TotalActions++;
            }
            if(action.levelInfo=='Level 2') {
              this.level2TotalActions++;
            }
            if(action.levelInfo=='Level 3') {
              this.level3TotalActions++;
            }
            if(this.actionStatus[status]=='N' && action.levelInfo == 'Level 1') {
              this.level1Count--;
              this.level1Completed=false;
            }
            if(this.actionStatus[status]=='N' && action.levelInfo == 'Level 2') {
              this.level2Count--;
              this.level2Completed=false;
            }
            if(this.actionStatus[status]=='N' && action.levelInfo == 'Level 3') {
              this.level3Count--;
              this.level3Completed=false;
            }
            if(action.levelInfo=='Level 1' && action.proofApproved) {
              this.level1TotalActionsApproved++;
              if(this.level1TotalActionsApproved==3) {
                this.level1Approved=true;
              }
            }
            if(action.levelInfo=='Level 2' && action.proofApproved) {
              this.level2TotalActionsApproved++;
              if(this.level2TotalActionsApproved==3) {
                this.level2Approved=true;
              }
            }
            if(action.levelInfo=='Level 3' && action.proofApproved) {
              this.level3TotalActionsApproved++;
              if(this.level3TotalActionsApproved==3) {
                this.level3Approved=true;
              }
            }
          }
        } 
      }
    })
  }

  showModalDialog(level) {
    this.displayModal = true;
    this.proofLevelSelected = level;
    this.getAllActions();
    //this.getAllAttachments();
  }

  closeModalDialog() {
    this.displayModal = false;
  }

  actionChecked(actionCode) {
    for(let action of this.actions) {
      if(actionCode == action.code) {
        if(action.proofChecked) {
          action.proofChecked=false;
        }
        else {
          action.proofChecked=true;
        }
      }
    }
  }

  actionPlanClicked(action) {
    if(this.display==action) {
      this.display='';
    }
    else {
      this.display=action;
    }
  }

  showActionPlans(level) {
    document.getElementById('level1').style.fontWeight='normal';
    document.getElementById('level2').style.fontWeight='normal';
    document.getElementById('level3').style.fontWeight='normal';
    this.levelSelected = level;
    let formattedLevel = level.replace(/\s/g, "").toLowerCase();
    document.getElementById(''+formattedLevel).style.fontWeight='bold';
  }

  markComplete(levelCode) {
    if(this.actionStatus[levelCode]=='N') {
      this.actionStatus[levelCode]='Y';
      this.http.put('http://localhost:8080/eco-amica-0.0.1-SNAPSHOT/service/updateMeasuresToComplete',this.actionStatus)
      .subscribe(res => {
        this.actionCompletedPrev=false;
        this.getAllActions();
        this.showActionCompletedDialog();
      })
    }
    else {
      this.actionCompletedPrev=true;
      this.showActionCompletedDialog();
    }
    
  }

  showActionCompletedDialog() {
    this.actionCompletedDialog=true;
  }

  closeActionCompleteDialog() {
    this.actionCompletedDialog=false;
  }

  fileUpload(event, action) {
    let submitData={
      hotelCode:'',
      level:'0',
      levelType:'',
      files:null
    };
    submitData.level=action.levelInfo.substring(6,7);
    submitData.levelType=action.levelType;
    let file: File = event.target.files[0];
    let formData:FormData = new FormData();
    formData.append('files', file, file.name);
    formData.append('hotelCode',this.hotelCode);
    formData.append('levelType',submitData.levelType);
    formData.append('level',submitData.level);
    if(action.proofRejected) {
      this.http.delete('http://localhost:8080/eco-amica-0.0.1-SNAPSHOT/service/deleteAttachment/'+action.attachmentId)
      .subscribe(res => {
        console.log(res);
        this.http.post('http://localhost:8080/eco-amica-0.0.1-SNAPSHOT/service/saveMeasures',formData)
        .subscribe(res => {
          this.getAllActions();
          //this.getAllAttachments();
          this.showFileConfirmDialog();
          this.sendMailToReviewer(submitData.level,this.hotelCode);
        })
      })
    }
    else {
      this.http.post('http://localhost:8080/eco-amica-0.0.1-SNAPSHOT/service/saveMeasures',formData)
      .subscribe(res => {
        this.getAllActions();
        //this.getAllAttachments();
        this.showFileConfirmDialog();
        this.sendMailToReviewer(submitData.level,this.hotelCode);
      })
    }
  }

  sendMailToReviewer(level,hotelCode) {
    let submitData={
      level:level,
      hotelCode:hotelCode
    }
    this.http.post('http://localhost:8080/eco-amica-0.0.1-SNAPSHOT/service/sendEmailToReviewer/'+level+'/'+hotelCode,submitData)
    .subscribe(res => {
    })
  }

  downloadFile(attachment) {
    window.open('http://localhost:8080/eco-amica-0.0.1-SNAPSHOT/service/downloadFile/'+attachment.attachment_id,attachment.fileName);
  }

  showFileConfirmDialog() {
    this.showFileConfirm=true;
  }

  hideFileConfirmDialog() {
    this.showFileConfirm=false;
  }

  viewProofs(level) {
    this.viewLevelProofs=level;
    this.viewProofModal=true;
  }

  hideProofs() {
    this.viewProofModal=false;
  }

}
