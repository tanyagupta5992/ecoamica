import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  nav(item) {
    if(item=='1') {
      document.getElementById('nav1').style.color='white';
      document.getElementById('nav2').style.color='#808080';
      document.getElementById('nav3').style.color='#808080';
      document.getElementById('nav5').style.color='#808080';
    }
    else if(item=='2') {
      document.getElementById('nav1').style.color='#808080';
      document.getElementById('nav2').style.color='white';
      document.getElementById('nav3').style.color='#808080';
      document.getElementById('nav5').style.color='#808080';
    }
    else if(item=='3') {
      document.getElementById('nav1').style.color='#808080';
      document.getElementById('nav2').style.color='#808080';
      document.getElementById('nav3').style.color='white';
      document.getElementById('nav5').style.color='#808080';
    }
    else if(item=='5') {
      document.getElementById('nav1').style.color='#808080';
      document.getElementById('nav2').style.color='#808080';
      document.getElementById('nav3').style.color='#808080';
      document.getElementById('nav5').style.color='white';
    }
    else {}
  }

}
