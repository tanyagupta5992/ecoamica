import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsumptionDataComponent } from './consumption-data.component';

describe('ConsumptionDataComponent', () => {
  let component: ConsumptionDataComponent;
  let fixture: ComponentFixture<ConsumptionDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsumptionDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsumptionDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
