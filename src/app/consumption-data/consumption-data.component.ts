import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-consumption-data',
  templateUrl: './consumption-data.component.html',
  styleUrls: ['./consumption-data.component.css']
})
export class ConsumptionDataComponent implements OnInit {

  constructor(private http:HttpClient) { }

  utilitySelected='Electricity';
  utilityId=1;
  hotelCode='TESTA';
  dateValue=new Date();
  monthSelected:any;
  yearSelected:any;
  usageData:any;
  usageText='';
  today=new Date();
  submitData={
    month: '',
    usageId: null,
    usageText: '',
    utilityType: '',
    year: ''
  }
  showSaveDialog:boolean;
  
  ngOnInit() {
    this.getUsageData();
  }

  utilityChosen(utility,id) {
    this.utilitySelected=utility;
    this.utilityId=id;
    this.getUsageData();
    document.getElementById('electricity').style.fontWeight='normal';
    document.getElementById('water').style.fontWeight='normal';
    document.getElementById('gas').style.fontWeight='normal';
    document.getElementById('plastic').style.fontWeight='normal';
    let formattedUtility = utility.toLowerCase();
    document.getElementById(''+formattedUtility).style.fontWeight='bold';
  }

  getUsageData() {
    this.usageData=null;
    this.usageText='';
    this.monthSelected = this.dateValue.getMonth();
    this.yearSelected = this.dateValue.getFullYear();
    this.http.get('http://localhost:8080/eco-amica-0.0.1-SNAPSHOT/service/getUsage/'+this.monthSelected+'/'+this.yearSelected+'/'+this.hotelCode)
    .subscribe(res => {
      this.usageData=res;
      if(this.usageData) {
        if(this.usageData.utilityType == this.utilitySelected) {
          this.usageText=this.usageData.usageText;
        }
      }
    })
  }

  save() {
    this.submitData={
      month: ''+this.monthSelected,
      usageId: null,
      usageText: this.usageText,
      utilityType: this.utilitySelected,
      year: ''+this.yearSelected
    }
    if(!this.usageData) {
      this.http.post('http://localhost:8080/eco-amica-0.0.1-SNAPSHOT/service/save/usage/'+this.utilityId+'/'+this.hotelCode,this.submitData)
      .subscribe(res => {
         this.getUsageData();
         this.showSaveDialog=true;
      })
    }  
    else {
      this.http.put('http://localhost:8080/eco-amica-0.0.1-SNAPSHOT/service/update/usage/'+this.monthSelected,this.submitData)
      .subscribe(res => {
         this.getUsageData();
         this.showSaveDialog=true;
      })
    }
  }

  closeSaveDialog() {
    this.showSaveDialog=false;
  }

}
