import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { GreenmeasuresComponent } from './greenmeasures/greenmeasures.component';
//import { TooltipModule } from 'primeng/components/tooltip/tooltip';
import { DropdownModule, CheckboxModule, DialogModule, FileUploadModule, TabViewModule, MenubarModule, PanelMenuModule, ListboxModule, OverlayPanelModule, MultiSelectModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ApprovalsComponent } from './approvals/approvals.component';
import { HttpClientModule } from '@angular/common/http';
import { ConsumptionDataComponent } from './consumption-data/consumption-data.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    GreenmeasuresComponent,
    ApprovalsComponent,
    ConsumptionDataComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    CheckboxModule,
    DropdownModule,
    DialogModule,
    BrowserAnimationsModule,
    FileUploadModule,
    TabViewModule,
    MenubarModule,
    PanelMenuModule,
    ListboxModule,
    OverlayPanelModule,
    MultiSelectModule,
    CalendarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
